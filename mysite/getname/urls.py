from django.urls import path
from . import views
app_name = 'getname'

urlpatterns = [
    path('', views.index,name='index'),
]