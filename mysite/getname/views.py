from django.shortcuts import render, redirect
from django.http import HttpResponseRedirect
from django.template import loader
from .forms import NameForm

# Create your views here.
def index(request):
    #template = loader.get_template('index.html')
    names = ["Long", "Ngan", "Van"]
    return render(request,'index.html',{'names':names})
    #return HttpResponse("Hello World")
    #return redirect('https://google.com')
